//
//  LessonSchedule.swift
//  Scedule
//
//  Created by Anton on 26.05.16.
//  Copyright © 2016 Loosers. All rights reserved.
//

import Foundation
import CoreData


class LessonSchedule: NSManagedObject {
    
    @NSManaged var teacher: String
    @NSManaged var time: String
    @NSManaged var subject: String
    @NSManaged var room: String


}
