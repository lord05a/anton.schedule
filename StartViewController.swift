//
//  StartViewController.swift
//  Scedule
//
//  Created by Anton on 26.05.16.
//  Copyright © 2016 Loosers. All rights reserved.
//

import UIKit

class StartViewController: UIViewController {
    
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    
    var isLoaded : Bool? = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //if let _isLoaded : Bool? = SaveLoader.LoadData("isLoaded") != nil {
          //  isLoaded = _isLoaded!
        //}
        
        
        
        if (isLoaded!)
        {
            //Schedule.grades = SaveLoader.LoadData("Schedule.Grades")!
            //Schedule.groups = SaveLoader.LoadData("Schedule.Groups")!
            return
        }
        
        indicator.startAnimating()

        let api : API = API()
        api.getJSON(api.gradesUrl)
        {
            gradesJson in
                api.jsonGradesParse(gradesJson)
            
                for grade in Schedule.grades
                {
                    api.getJSON(api.groupsUrl + "\(grade.0)")
                    {
                        groupsJson in
                            api.jsonGrageGroupsParse(groupsJson)
                    
                            api.terminationCount = Schedule.groups.count
                            for group in groupsJson!.arrayValue
                            {
                                api.getJSON(api.shcheduleUrl + "\(group["id"])")
                                {
                                    scheduleJson in
                                        api.jsonShceduleParse(
                                            scheduleJson,
                                            groupid: group["id"].intValue,
                                            terminatingFunction: self.loadingDone)
                                }
                            }
                    }
                }
        }
    }

    @IBAction func loadingDone(){
        isLoaded = true
        //SaveLoader.SaveData(isLoaded!, key: "isLoaded")
        
        //SaveLoader.SaveData(Schedule.grades, key: "Schedule.Grades")
        //SaveLoader.SaveData(Schedule.groups, key: "Schedule.Groups")
        //SaveLoader.SaveData(Schedule.getAllGradeGroupPairs(), key: "Schedule.Main")
        
        ViewController.gradesIds = Schedule.grades.keys.map{ x -> Int in x }
        ViewController.groupsIds = Schedule.groups.keys.map{ x -> Int in x }
        
        let vc = self.storyboard!.instantiateViewControllerWithIdentifier("MainView") as! ViewController
        self.showViewController(vc, sender: self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    

}
