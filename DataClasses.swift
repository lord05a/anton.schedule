//
//  DataClasses.swift
//  Scedule
//
//  Created by Anton on 26.05.16.
//  Copyright © 2016 Loosers. All rights reserved.
//

import Foundation

class Grade : NSObject
{
    var number = Int()
    var degree = String()
}

class Group : NSObject
{
    var group_id = Int()
    var name = String()
    var number = Int()
    var grade_id = Int()
}

class LessonTime : NSObject
{
    var dayOfWeek = Int()
    var start = String()
    var end = String()
    var week = Week.FULL
}

enum Week {
    case FULL
    case UPPER
    case LOWER
}

class Class : NSObject
{
    var time = LessonTime()
    var subjname = String()
    var teachername = String()
    var roomname = String()
    var weekname : Week = Week.FULL
}


// Global static class
class Schedule : NSObject
{
    // Global variables
    static var grades = [Int : Grade]()
    static var groups = [Int : Group]()
    static var oneGroupSchedule = [Int : [Class]]()
    
    static func getAllGradeGroupPairs () -> [Int : (Grade, Group)]
    {
        var result = [Int : (Grade, Group)]()
        
        let allGroups = self.groups.map{
                (id, group) -> (Int, Group) in
                return (Int,Group)(id, group)
        }

        
        for _group in allGroups
        {
            result[_group.0] = (self.grades[_group.1.grade_id]!, _group.1)
        }
        
        return result
    }
    
    static func getGroupId (grade : Int, group : Int) -> Int
    {
        let all = getAllGradeGroupPairs()
        
        for pair in all
        {
            if pair.1.0.number == grade && pair.1.1.number == group
            {
                return pair.0
            }
        }
        
        return -1
    }
    
}

class NamedDaysOfWeek : NSObject
{
    static func dayIs(day : Int) -> String
    {
        switch day {
        case 2:
            return "Понедельник"
        case 3:
           return "Вторник"
        case 4:
           return "Среда"
        case 5:
           return "Четверг"
        case 6:
           return "Пятница"
        case 7:
           return "Суббота"
        case 1:
           return "Воскресенье"
        default:
           return "Unknown day"
        }
    }
}












