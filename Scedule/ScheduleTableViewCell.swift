//
//  ScheduleTableViewCell.swift
//  Scedule
//
//  Created by Loosers on 25.05.16.
//  Copyright © 2016 Loosers. All rights reserved.
//

import UIKit

class ScheduleTableViewCell: UITableViewCell {

    @IBOutlet weak var CellTime: UILabel!
    @IBOutlet weak var CellSubject: UILabel!
    @IBOutlet weak var CellTeacher: UILabel!
    @IBOutlet weak var CellRoom: UILabel!
    @IBOutlet weak var CellWeek: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    internal func setUp(time : String, subject : String, teacher : String, room : String, week : String) {
        CellTime.text! = time
        CellSubject.text! = subject
        CellTeacher.text! = teacher
        CellRoom.text! = room
        CellWeek.text! = week
    }

}
