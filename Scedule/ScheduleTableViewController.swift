//
//  ScheduleTableViewController.swift
//  Scedule
//
//  Created by Loosers on 25.05.16.
//  Copyright © 2016 Loosers. All rights reserved.
//

import UIKit
import CoreData

class ScheduleTableViewController: UITableViewController, NSFetchedResultsControllerDelegate {
    
    @IBOutlet weak var weekIsLow: UIBarButtonItem!
    @IBOutlet weak var dayItem: UINavigationItem!
    
    @IBOutlet var pageNumofWeek: UITableView!
    
    var groupId : Int = 0
    var isEmptySchedule : Bool = false
    var day : Int = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        day = getDayOfWeek()!
        if (day > 7) { day = 2 }
        
        dayItem.title? = NamedDaysOfWeek.dayIs(getDayOfWeek()!)
    }

    
    @IBAction func capChangeSchedule(sender: UISwitch) {
        
        day += 1
        if (day > 7) { day = 2 }
        
        dayItem.title? = NamedDaysOfWeek.dayIs(day)
        
        isEmptySchedule = false
        
        tableView.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        let count = getClasses().count
        if count == 0
        {
            isEmptySchedule = true
            return 1
        }
        else
        {
            return count
        }
    }
    
    func getDayOfWeek()->Int? {
        let todayDate = NSDate()
        let myCalendar = NSCalendar(calendarIdentifier: NSGregorianCalendar)
        let myComponents = myCalendar?.components(.NSWeekdayCalendarUnit, fromDate: todayDate)
        let weekDay = myComponents?.weekday
        return weekDay!
    }
    
    func getWeekType(week : Week) -> String
    {
        switch week {
        case Week.FULL:
            return "Все дни"
        case Week.LOWER:
            return "Нижняя неделя"
        case Week.UPPER:
            return "Верхняя неделя"
        default:
            "Unknown week type"
        }
    }
    
    func getClasses() -> [Class]
    {
        if groupId == -1
        {
            return [Class()]
        }
        return Schedule.oneGroupSchedule[groupId]!.filter{
            $0.time.dayOfWeek == (day - 2 + 7) % 7
            }.sort{
                (a, b) -> Bool in
                let x = a.time.start.substringToIndex(a.time.start.startIndex.advancedBy(2))
                let y = b.time.start.substringToIndex(b.time.start.startIndex.advancedBy(2))
                
                return Int(x)! < Int(y)!
        }
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("ScheduleCell", forIndexPath: indexPath) as! ScheduleTableViewCell

        if isEmptySchedule == true
        {
            cell.setUp("", subject: "Сегодня у Вас нет занятий", teacher: "Удачного дня", room: "", week: "")
        }
        else if groupId == -1
        {
            cell.setUp("", subject: "Такой группы не существует", teacher: "Не переживайте", room: "", week: "")
        }
        else
        {
            let _class = getClasses()[indexPath.row]
            
            cell.setUp(
                "\(_class.time.start) – \(_class.time.end)",
                subject: _class.subjname,
                teacher: _class.teachername,
                room: _class.roomname,
                week: getWeekType(_class.time.week)
            )
        }
        return cell
    }
    
    /*
     override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
     let cell = tableView.dequeueReusableCellWithIdentifier("reuseIdentifier", forIndexPath: indexPath)
     
     // Configure the cell...
     
     return cell
     }
     */
    
    /*
     // Override to support conditional editing of the table view.
     override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    /*
     // Override to support editing the table view.
     override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
     if editingStyle == .Delete {
     // Delete the row from the data source
     tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
     } else if editingStyle == .Insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }
     */
    
    /*
     // Override to support rearranging the table view.
     override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    
     // MARK: - Navigation
    
}
