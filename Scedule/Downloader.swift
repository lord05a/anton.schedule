//
//  Downloader.swift
//  Scedule
//
//  Created by Loosers on 25.05.16.
//  Copyright © 2016 Loosers. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class API: NSObject {
    let gradesUrl    = "http://users.mmcs.sfedu.ru:3000/grade/list"
    let groupsUrl    = "http://users.mmcs.sfedu.ru:3000/group/list/"
    let shcheduleUrl = "http://users.mmcs.sfedu.ru:3000/schedule/group/"
    
    var terminationCount = Int()
    var currentNumber = 0
    
    func getJSON(urlPath: String, completionHendler: JSON? -> Void) {
        Alamofire.request(.GET, urlPath)
            .responseJSON { response in
                switch response.result {
                case .Success(let value):
                    let result = JSON(value)
                    completionHendler(result)
                case .Failure:
                    completionHendler(nil)
                }
        }
    }
    
    func jsonGradesParse(isJson : JSON?)
    {
        let json = isJson
        if (json == nil)
        {
            return
        }
        
        Schedule.grades = [Int : Grade]()
        for grade in json!.arrayValue
        {
            let g = Grade()
            let key = grade["id"].intValue
            g.number = grade["num"].intValue
            g.degree = grade["degree"].stringValue
            Schedule.grades[key] = g
        }
    }
    
    func jsonGrageGroupsParse(isJson : JSON?)
    {
        let json = isJson
        if (json == nil)
        {
            return
        }
        Schedule.groups = [Int : Group]()
        for group in json!.arrayValue
        {
            let g = Group()
            let id = group["id"].intValue
            g.number = group["num"].intValue
            g.name = group["name"].stringValue
            g.grade_id = group["gradeid"].intValue
            Schedule.groups[id] = g
        }
    }
    
    func jsonShceduleParse(isJson : JSON?, groupid : Int, terminatingFunction : () -> Void)
    {
        let json = isJson
        if (json == nil)
        {
            return
        }
        
        var lessonTimes = [Int : LessonTime]()
        for time in json!["lessons"].arrayValue
        {
            lessonTimes[time["id"].intValue] = timeslotParse(time["timeslot"].stringValue)
        }
        
        Schedule.oneGroupSchedule[groupid] = [Class]()
        for curricula in json!["curricula"].arrayValue
        {
            let _class = Class()
            _class.subjname = curricula["subjectname"].stringValue
            _class.teachername = curricula["teachername"].stringValue
            _class.roomname = curricula["roomname"].stringValue
            _class.time = lessonTimes[curricula["lessonid"].intValue]!
            Schedule.oneGroupSchedule[groupid]!.append(_class)
        }
        
        currentNumber += 1
        if terminationCount == currentNumber
        {
           /* let delay = 1.3 * Double(NSEC_PER_SEC)
            let time = dispatch_time(DISPATCH_TIME_NOW, Int64(delay))
            dispatch_after(time, dispatch_get_main_queue()) {
                // After 2 seconds this line will be executed*/

           // }
                terminatingFunction()
        }
    }
    
    func timeslotParse(timeSlot : String) -> LessonTime {
        let time = LessonTime()
        
        var ts = timeSlot.characters
        ts.removeFirst()
        ts.removeLast()
        
        let arr = ts.split(",", maxSplit: 4, allowEmptySlices: false).map { elem -> String in return String(elem) }
        
        time.dayOfWeek = Int(arr[0])!
        time.start = arr[1].substringToIndex(arr[1].startIndex.advancedBy(5))
        time.end = arr[2].substringToIndex(arr[2].startIndex.advancedBy(5))
        
        switch arr[3] {
        case "upper":
            time.week = Week.UPPER
        case "lower":
            time.week = Week.LOWER
        default:
            time.week = Week.FULL
        }
        
        return time
    }
}