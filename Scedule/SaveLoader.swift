//
//  SaveLoader.swift
//  Scedule
//
//  Created by Anton on 27.05.16.
//  Copyright © 2016 Loosers. All rights reserved.
//

import Foundation

class SaveLoader: NSCoder {
    
    static func SaveData<ItemType>(item: ItemType, key : String) {
        
        let data = NSKeyedArchiver.archivedDataWithRootObject(item as! AnyObject)
        NSUserDefaults.standardUserDefaults().setObject(data, forKey: key)
    }
    
    static func LoadData<ItemType>(key : String) -> ItemType? {
        
        if let data = NSUserDefaults.standardUserDefaults().objectForKey(key) as? NSData {
            let result = NSKeyedUnarchiver.unarchiveObjectWithData(data)
            return result as! ItemType?
        }
        
        return nil
    }
}
