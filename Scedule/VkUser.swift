//
//  VkUser.swift
//  Scedule
//
//  Created by Anton on 26.05.16.
//  Copyright © 2016 Loosers. All rights reserved.
//

import Foundation
import CoreData


class VkUser: NSManagedObject {

    @NSManaged var city: String
    @NSManaged var first_name: String
    @NSManaged var last_name: String
    @NSManaged var country: String

}
