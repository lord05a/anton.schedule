//
//  ViewController.swift
//  Scedule
//
//  Created by Loosers on 22.05.16.
//  Copyright © 2016 Loosers. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController {
    
    @IBOutlet weak var courseButs: UIStackView!
    @IBOutlet weak var groupButs: UIStackView!
    
    var numberCourse = -1
    //var numCourseButton : UIButton? = nil
    
    var numberGroup = -1
    //var numGroupButton : UIButton? = nil
    
    // чтобы передать в таблицу расписания
    var groupid : Int = 0
    @IBOutlet weak var getScheduleButton: UIButton!
    
    //устанавливаем из первой страницы – загрузчика
    static var gradesIds = [Int]()
    static var groupsIds = [Int]()
    
    @IBAction func changeCourse(sender: UIButton) {
        
        numberCourse = Int(sender.currentTitle!)!
        
        for button in courseButs.arrangedSubviews
        {
            if let b = button as? UIButton
            {
                    b.backgroundColor = UIColor.whiteColor()
            }
        }
        
        sender.backgroundColor = UIColor.cyanColor()
        
        activateScheduleButton()
    }
    
    @IBAction func changeGroup(sender: UIButton) {
        
        numberGroup = Int(sender.currentTitle!)!
        
        for button in groupButs.arrangedSubviews
        {
            if let b = button as? UIButton
            {
                b.backgroundColor = UIColor.whiteColor()
            }
        }
        
        sender.backgroundColor = UIColor.cyanColor()
        
        activateScheduleButton()
        
    }
    
    @IBOutlet weak var goToScheduleButton: UIButton!
    
    func activateScheduleButton() {
        if numberGroup > -1 && numberCourse > -1 {
            goToScheduleButton?.enabled = true
            goToScheduleButton.setTitle("ПОКАЗАТЬ РАСПИСАНИЕ", forState: UIControlState.Normal)
            
            SaveLoader.SaveData(numberGroup, key: "Group")
            SaveLoader.SaveData(numberCourse, key: "Grade")
        }
    }
    
    
    @IBAction func DropData(sender: UIButton)
    {
        for button in courseButs.arrangedSubviews
        {
            if let b = button as? UIButton
            {
                b.backgroundColor = UIColor.whiteColor()
            }
        }
        
        for button in groupButs.arrangedSubviews
        {
            if let b = button as? UIButton
            {
                b.backgroundColor = UIColor.whiteColor()
            }
        }

        numberCourse = -1
        numberGroup = -1
        
        
        
        goToScheduleButton.setTitle("ВЫБЕРИТЕ КУРС И ГРУППУ", forState: UIControlState.Normal)
    
        SaveLoader.SaveData(numberGroup, key: "Group")
        SaveLoader.SaveData(numberCourse, key: "Grade")
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        numberGroup = SaveLoader.LoadData("Group")!
        numberCourse = SaveLoader.LoadData("Grade")!
        
        self.navigationItem.hidesBackButton = true
        
        for button in courseButs.arrangedSubviews
        {
            if let b = button as? UIButton
            {
                if b.titleLabel!.text ==  "\(numberCourse)"
                {
                    b.backgroundColor = UIColor.cyanColor()
                }
            }
        }
        
        for button in groupButs.arrangedSubviews
        {
            if let b = button as? UIButton
            {
                if b.titleLabel!.text ==  "\(numberGroup)"
                {
                    b.backgroundColor = UIColor.cyanColor()
                }
            }
        }
        
        if numberGroup == -1 || numberCourse == -1
        {
            goToScheduleButton.setTitle("ВЫБЕРИТЕ КУРС И ГРУППУ", forState: UIControlState.Normal)
        }
        else
        {
            
            activateScheduleButton()
        }
    }
    
    func createButton () {
        let button = UIButton();
        button.setTitle("Add", forState: .Normal)
        button.setTitleColor(UIColor.blueColor(), forState: .Normal)
        button.frame = CGRectMake(15, -50, 200, 100)
        self.courseButs.addSubview(button)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK : Navigation
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if getScheduleButton === sender {
            groupid = Schedule.getGroupId(numberCourse, group: numberGroup)
            if let destination = segue.destinationViewController as? ScheduleTableViewController
            {
                destination.groupId = groupid
            }
        }
    }

}

